if [[ ! $(whoami) == 'root' ]];
then
	echo 'run script as root'
else
	echo options usbhid quirks=0xfeed:0x6060:0x20000000 >> /etc/modprobe.d/local.conf
	update-initramfs -k all -u
	echo 'now reboot'
fi 
