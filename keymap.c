
#include QMK_KEYBOARD_H
#define MEDIA_KEY_DELAY 10
#define MODS_SHIFT  (get_mods() & MOD_BIT(KC_LSHIFT) || get_mods() & MOD_BIT(KC_RSHIFT))
#define MODS_CTRL  (get_mods() & MOD_BIT(KC_LCTL) || get_mods() & MOD_BIT(KC_RCTRL))
#define MODS_ALT (get_mods() & MOD_BIT(KC_LALT) || get_mods() & MOD_BIT(KC_RALT))

extern keymap_config_t keymap_config;

enum planck_layers {
  _QWERTY,
  _LEFT ,
  _RIGHT,
  _ADJUST,
  _UNICODE,
  _MOUSE,
  _GAME_SELECT,
  _GAME_DEFAULT,
  _GAME_NUM
};

enum combos {
	JK_ESC, 
	UI_ALT
};

const uint16_t PROGMEM jk_combo[] = {KC_J, KC_K, COMBO_END};
const uint16_t PROGMEM ui_combo[] = {KC_U, KC_I, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
  [JK_ESC] = COMBO(jk_combo, KC_ESC),
  [UI_ALT] = COMBO(ui_combo, KC_LALT)
};

enum planck_keycodes {
  DELAYER = SAFE_RANGE,
  PARENS,
  BRACKS,
  CURLBS,
  D_CBRS,
  D_CBR_O,
  D_CBR_C,
  DD_QUOT,
  D_QUOT,
  LTGT,
  GITPUSH,
  GITPULL,
  GITCOMM,
  GITSTAT,
  GITADDU,
  GITCHCK
};

// Defines for task manager and such
#define CALTDEL LCTL(LALT(KC_DEL))
#define TSKMGR LCTL(LSFT(KC_ESC))
#define DSK_LFT LGUI(LCTL(KC_LEFT))
#define DSK_RT  LGUI(LCTL(KC_RIGHT))
#define ALTTAB  LALT(KC_TAB)
#define OSSPACE LGUI(KC_SPACE)


// Defines layer-tap keys
#define LEFT     LT( _LEFT   , KC_TAB     )
#define RIGHT    LT( _RIGHT  , KC_DEL      )
#define BSPC     MT( MOD_LCTL , KC_BSPC    )
#define ADJUST   MO( _ADJUST) // is activated using layerstate change code
#define MOUSE    MO( _MOUSE)
#define GAMESEL MO( _GAME_SELECT)
#define ENTER    MT( MOD_LALT , KC_ENTER    )
#define SPACE    KC_SPACE 
#define GUI      KC_LGUI
#define SHIFT    KC_LSFT 
#define UNIC  MO( _UNICODE)

#define UCae    UC(0x00E6) // æ
#define UCoe    UC(0x00F8) // ø
#define UCaa    UC(0x00E5) // å
#define UCAE    UC(0x00C6) // Æ
#define UCOE    UC(0x00D8) // Ø
#define UCAA    UC(0x00C5) // Å 


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Qwerty
 * ,-----------------------------------------------------------------------------------.
 * |   Q  |   W  |   E  |   R  |   T  |  <   |   >  |   Y  |   U  |   I  |   O  |   P  |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |   A  |   S  |   D  |   F  |   G  |   (  |   )  |   H  |   J  |   K  |   L  |   ;  |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |   Z  |   X  |   C  |   V  |   B  |   {  |   }  |   N  |   M  |   ,  |   .  |   /  |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      | GUI  | LEFT | BSPC | SHIFT|ENTER |SPACE | RIGHT| TAB  |      | DELAYER|
 * `-----------------------------------------------------------------------------------'
 */
[_QWERTY] = LAYOUT_planck_grid(
    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,  _______, DYN_REC_STOP,   KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,
    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,  _______, _______,   KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN,
    KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,  MOUSE  , GAMESEL,   KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,
    XXXXXXX, XXXXXXX, GUI,     LEFT ,   BSPC,    SHIFT,   ENTER,   SPACE,   RIGHT,   UNIC   , XXXXXXX, DELAYER
),

[_LEFT ] = LAYOUT_planck_grid(
    KC_ESC,  _______, KC_LCBR, KC_RCBR, _______, DM_REC1, DM_PLY1, KC_PLUS,    KC_7,   KC_8,    KC_9,    KC_0   ,
    _______, _______, KC_LPRN, KC_RPRN, _______, DM_REC2, DM_PLY2, KC_MINUS,   KC_4,   KC_5,    KC_6,    KC_EQUAL,
    _______, _______, KC_LBRC, KC_RBRC, _______, _______, _______, KC_ASTR,    KC_1,   KC_2,    KC_3,    _______, 
    _______, _______, _______, _______, _______, _______, _______, _______,  _______, _______, _______, _______
),

[_RIGHT] = LAYOUT_planck_grid(
    KC_CIRC, KC_AMPR, KC_TILD, KC_PIPE, KC_BSLS, GITSTAT, GITADDU, KC_HOME, KC_PGDN, KC_PGUP, KC_END,  KC_PSCR,
    KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, GITCOMM, GITCHCK, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, KC_BSPC,
    KC_DQUO, KC_QUOT, KC_UNDS, KC_GRV,  _______, GITPUSH, GITPULL, _______, _______, _______, _______, _______,
    _______, _______, _______, _______,  _______, _______, _______, _______, _______, _______, _______, _______
),

[_ADJUST] = LAYOUT_planck_grid(
    _______, _______, CURLBS,  _______, _______, _______, _______, _______, KC_F7,   KC_F8,   KC_F9,   KC_F10,
    _______, _______, PARENS,  _______, _______, _______, _______, _______,  KC_F5,   KC_F6,   KC_F7,   KC_F11,
    DD_QUOT, D_QUOT,  BRACKS,  _______, _______, _______, _______, _______, KC_F1,   KC_F2,   KC_F3,   KC_F12,
    RESET,   _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, RESET
),

[_UNICODE] = LAYOUT_planck_grid(
    UNICODE_MODE_WINC , UNICODE_MODE_LNX, _______, _______, _______, _______, _______, _______, _______, UCAA   , _______, UCaa,
    _______, _______, _______, _______, _______, _______, _______, _______, UCAE   , UCOE   , UCae   , UCoe,
    _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
),

[_MOUSE] = LAYOUT_planck_grid(
    _______, _______, _______, _______, _______, _______, _______, KC_ACL2, _______, KC_MS_U, _______, _______,
    _______, _______, _______, _______, _______, _______, _______, KC_ACL1, KC_MS_L, KC_BTN3, KC_MS_R, KC_BTN2,
    _______, _______, _______, _______, _______, _______, _______, KC_ACL0, _______, KC_MS_D, _______, _______,
    _______, _______, _______, _______, _______, _______, _______, KC_BTN1, _______, _______, _______, _______
),

[_GAME_SELECT] = LAYOUT_planck_grid(
    _______, _______, _______         ,	_______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______, _______, TG(_GAME_DEFAULT), _______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______         , _______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______         , _______, _______, _______, _______, _______, _______, _______, _______, _______
),

[_GAME_DEFAULT] = LAYOUT_planck_grid(
    KC_TAB ,    KC_Q,    KC_W,    KC_E,          KC_R,    KC_F7, _______, _______, _______, _______, _______, _______, 
    KC_LSFT,    KC_A,    KC_S,    KC_D,          KC_F,    KC_F3, _______, _______, _______, _______, _______, _______, 
    KC_LCTL,    KC_Z,    KC_X,    KC_C,          KC_V,    KC_F5, _______, _______, _______, _______, _______, _______, 
    _______, _______, _______, MO(_GAME_NUM),KC_SPACE, _______, _______, _______, _______, _______, _______, _______
),

[_GAME_NUM] = LAYOUT_planck_grid(
    _______, _______, _______ , _______, _______, _______, _______, _______, _______, _______, _______, _______,
    KC_1   , KC_2   , KC_3    , KC_4   , KC_5   , _______, _______, _______, _______, _______, _______, _______,
    KC_6   , KC_7   , KC_8    , KC_9   , KC_0   , _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______ , _______, _______, _______, _______, _______, _______, _______, _______, _______
)

};

layer_state_t layer_state_set_user(layer_state_t state) {
	return update_tri_layer_state(state, _LEFT, _RIGHT, _ADJUST);
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) { 
    case DELAYER:
      if (record->event.pressed) {
		  layer_clear();
      }
      break;
    case PARENS:
      if (record->event.pressed) {
        SEND_STRING("()" SS_TAP(X_LEFT) );
      }
      break;
    case BRACKS:
      if (record->event.pressed) {
        SEND_STRING("[]" SS_TAP(X_LEFT) );
      }
      break;
    case LTGT:
      if (record->event.pressed) {
        SEND_STRING("<>" SS_TAP(X_LEFT) );
      }
      break;
    case CURLBS:
      if (record->event.pressed) {
        SEND_STRING("{}" SS_TAP(X_LEFT) );
      }
      break;
    case D_CBRS:
      if (record->event.pressed) {
        SEND_STRING("{{}}" SS_TAP(X_LEFT) SS_TAP(X_LEFT) );
      }
      break;
    case D_QUOT:
      if (record->event.pressed) {
        SEND_STRING("''" SS_TAP(X_LEFT) );
      }
      break;
    case DD_QUOT:
      if (record->event.pressed) {
        SEND_STRING("\"\"" SS_TAP(X_LEFT) );
      }
      break;
    case D_CBR_O:
      if (record->event.pressed) {
        SEND_STRING("{{");
      }
      break;
    case D_CBR_C:
      if (record->event.pressed) {
        SEND_STRING("}}");
      }
      break;
/*************
 *************
 *  git 
 *************
 *************
 */
	case GITPUSH:
      if (record->event.pressed) {
      	SEND_STRING("git push");
      }
	  break;
	case GITPULL:
	  if (record->event.pressed) {  
	  SEND_STRING("git pull");
      }
	  break;
	case GITADDU:
	  if (record->event.pressed) {  
	  SEND_STRING("git add -u");
      }
	  break;
	case GITCOMM:
	  if (record->event.pressed) {  
	  SEND_STRING("git commit -m \"\""SS_TAP(X_LEFT));
      }
	  break;
	case GITSTAT:
	  if (record->event.pressed) {  
	  SEND_STRING("git status");
      }
	  break;
	case GITCHCK:
	  if (record->event.pressed) {  
	  SEND_STRING("git checkout ");
      }
	  break;
  }
  return true;
};


void encoder_update_user(uint8_t index, bool clockwise) {
  uint16_t held_keycode_timer = timer_read();
  if (MODS_ALT) {
	// base layer, alt active
  if (clockwise) {
    register_code(KC_TAB);
    unregister_code(KC_TAB);
  } else {
    register_code(KC_LSHIFT);
    register_code(KC_TAB);
    unregister_code(KC_TAB);
    unregister_code(KC_LSHIFT);
  }	
  
  } else if (MODS_CTRL) {
	if (clockwise) {
		tap_code(KC_Y);
	} else {
		tap_code(KC_Z);
	}

  } else if (biton32(layer_state) == _RIGHT) {
	  if (clockwise) {
		  tap_code(KC_DEL);
	  } else {
		  tap_code(KC_BSPC);
	  }

  } else {
	 //default, base layer, no mods 
  if (clockwise) {
    register_code(KC_VOLU);
    while (timer_elapsed(held_keycode_timer) < MEDIA_KEY_DELAY) {
      // no-op
    }
    unregister_code(KC_VOLU);
  } else {
    register_code(KC_VOLD);
    while (timer_elapsed(held_keycode_timer) < MEDIA_KEY_DELAY) {
      // no-op
    }
    unregister_code(KC_VOLD);
  }	
}	
};

	/*
	 *
   if (muse_mode) {
    if (IS_LAYER_ON(_RAISE)) {
      if (clockwise) {
        muse_offset++;
      } else {
        muse_offset--;
      }
    } else {
      if (clockwise) {
        muse_tempo+=1;
      } else {
        muse_tempo-=1;
      }
    }
  } else {
    if (clockwise) {
      #ifdef MOUSEKEY_ENABLE
        register_code(KC_MS_WH_DOWN);
        unregister_code(KC_MS_WH_DOWN);
      #else
        register_code(KC_PGDN);
        unregister_code(KC_PGDN);
      #endif
    } else {
      #ifdef MOUSEKEY_ENABLE
        register_code(KC_MS_WH_UP);
        unregister_code(KC_MS_WH_UP);
      #else
        register_code(KC_PGUP);
        unregister_code(KC_PGUP);
      #endif
    }
  } 
}*/

